
public class Submarinos {
	private String Marca="";
	private String Modelo="";
	private String Matricula="";
	private String FechaCreacion = "";
	private int Rumbo=0;
	private float X= (float) 0.0;
	private float Y = (float) 0.0;
	private float Z = (float) 0.0;
	private int velocidad = 0;
	private boolean periscopi=true;
	private boolean motor = true;
	private boolean amarrat = false;
	private boolean bomba = false;
	private boolean compuertas = false;
	
	public Submarinos(String Marca,String Modelo, String Matricula, float X,float Y,float Z,int velocidad, boolean periscopi, boolean motor, boolean amarrat, String FechaCreacion) {
		this.Marca = Marca;
		this.Modelo=Modelo;
		this.Matricula=Matricula;
		this.X = X;
		this.Y = Y;
		this.Z = Z;
		this.velocidad = velocidad;
		this.periscopi = periscopi;
		this.motor = motor;
		this.amarrat = amarrat;
		this.bomba = false;
		this.compuertas = false;
		this.Rumbo =0;
		this.FechaCreacion = FechaCreacion;
		
		Syncronizar();
	}
	
	//____Setters____
	public void setMarca(String Nom) {
		this.Marca = Nom;
	}
	
	public void setModelo(String Modelo) {
		this.Modelo = Modelo;
	}
	
	public void setMatricula(String Matricula) {
		this.Matricula=Matricula;
	}
	
	public void setX(float X) {
		this.X=X;
	}
	
	public void setY(float Y) {
		this.Y=Y;
	}
	
	public void setZ(float Z) {
		this.Z=Z;
	}
	
	public void setVelocidad(int velocidad) {
		this.velocidad=velocidad;
	}
	
	public void setPeriscopi(boolean periscopi) {
		this.periscopi=periscopi;
	}
	
	public void setMotor(boolean motor) {
		this.motor=motor;
	}
	
	public void setAmarrat(boolean amarrat) {
		this.amarrat=amarrat;
	}
	
	public void setBomba(boolean bomba) {
		this.bomba=bomba;
	}
	
	public void setCompuertas(boolean compuertas) {
		this.compuertas=compuertas;
	}
	public void setRumbo(int rumbo) {
		this.Rumbo = rumbo;
	}
	
	public void setFecha(String Fecha) {
		this.FechaCreacion = Fecha;
	}
	
	//______Getters______
	public String getMarca() { return (this.Marca); }
	public String getModelo() { return (this.Modelo); }
	public String getMatricula() { return (this.Matricula); }
	public int getRumbo() { return (this.Rumbo); }
	public float getX() { return (this.X); }
	public float getY() { return (this.Y); }
	public float getZ() { return (this.Z); }
	public int getVelocidad() { return (this.velocidad); }
	public boolean getPeriscopi() { return (this.periscopi); }
	public boolean getMotor() { return (this.motor); }
	public boolean getAmarrat() { return (this.amarrat); }
	public boolean getBomba() { return (this.bomba); }
	public boolean getCompuertas() { return (this.compuertas); }
	public String getFechaCreacion() { return (this.FechaCreacion); } 
	
	
	//- ---- SYNC ---
	public void Syncronizar() {
		String json_submarino = "{ \"submarino\": {\"Marca\": \""+this.Marca+"\",\"Modelo\": \""+this.Modelo+"\",\"Matricula\": \""+Juego.nombreJugador+"."+this.Matricula+"\",\"X\": \""+this.X+"\",\"Y\": \""+this.Y+"\",\"Z\": \""+this.Z+"\",\"velocidad\": \""+this.velocidad+"\",\"periscopi\": \""+this.periscopi+"\",\"motor\": \""+this.motor+"\",\"amarrat\": \""+this.amarrat+"\",\"bomba\": \""+this.bomba+"\",\"compuertas\": \""+this.compuertas+"\",\"Rumbo\": \""+this.Rumbo+"\",\"Fecha\": \""+this.FechaCreacion+"\"}}";
		Juego.MMOSYNC.GuardarSubmarinos(Juego.idJugador,Juego.tokenSesion,json_submarino);
	}
	
	public void Borrar() {
		String json_submarino = "{ \"submarino\": {\"Matricula\": \""+Juego.nombreJugador+"."+this.Matricula+"\"}}";
		Juego.MMOSYNC.BorrarSubmarino(Juego.idJugador,Juego.tokenSesion,json_submarino);
	}
}
