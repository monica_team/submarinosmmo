import java.util.Scanner;


public class Juego {
	static Scanner sc;
	static Squad squadJugador;
	static Audio audio;
	static Scanner scTMP;
	static int idJugador=0;
	static String nombreJugador="";
	static mmoSync MMOSYNC;
	static String tokenSesion;
	
	
	
	public static void main(String[] args) {
		//Un poco de musica para animar el ambiente. Que no se diga que el mar esta aburrido
		Audio.playClip();
		
		//Menu de login!
		MenuLogin();
		
		
	}
	
	public static void MenuLogin() {
		String NombreUsuario="";
		String Contra="";
		String respuesta;
		
		
		scTMP = new Scanner(System.in);
		System.out.println("SubmarinosMMO_______[INICIO DE SESION]");
		System.out.println("Escribe tu usuario para iniciar sesion:");
		NombreUsuario = scTMP.next();
		if(NombreUsuario != "") {
			System.out.println("Escribe tu contraseña para iniciar sesion:");
			Contra = scTMP.next();
			System.out.println("Comprobando datos....");
			MMOSYNC = new mmoSync();
			
			respuesta = MMOSYNC.iniciarSesion(NombreUsuario, Contra);
			String[] parts = respuesta.split(",");
			
			nombreJugador = NombreUsuario;
			idJugador = Integer.parseInt(parts[0]);
			tokenSesion = parts[1];
			
			if(idJugador > 0) {
				System.out.println("Bienvenido: "+NombreUsuario);
				//Instanciamos la clase principal para no sobreescribirla
				squadJugador = new Squad(NombreUsuario);
				String Submarinos = MMOSYNC.CargarSubmarinos(idJugador, tokenSesion);
				MMOSYNC.IniciarTimerSesion(idJugador,tokenSesion);
				squadJugador.CargarSubmarinos(Submarinos);
				
				// Instancio el scanner
				sc = new Scanner(System.in);
				
				//Inicio el menu
				Menu();
			} else {
				System.out.println("El nombre de usuario/contraseña es incorrecto.");
				//INICIO DE SESION INCORRECTO - MANDAMOS DE VUELTA
				MenuLogin();
			}
			
		}
		
	}
	
	public static void Menu() {
		int opcio=0;
		
		do {
			System.out.println("Entra una opcio de les seg.:");
			System.out.println("1. Manejar el SQUAD");
			System.out.println("2. Manejar submarino");
			System.out.println("3. Mantenimento submarino");
			System.out.println("4. Espacio marino");
			System.out.println("5. Salir del juego");
			
			opcio = sc.nextInt();
			
			switch(opcio) {
				case 1:
					ManejarSquad();
					break;
				case 2:
					ManejarSubmarino();
					break;
				case 3:
					ComprobarEstadoMarino();
					break;
				case 4:
					MostrarEstadoMar();
					break;
			}
			
		} while(opcio != 5);
		System.out.println("Hasta otra!");
		System.exit(0);
	}
	
	public static void ManejarSquad() {
		sc = new Scanner(System.in);
		int opcio=0;
		
		
		String Marca="";
		String Modelo="";
		String Matricula="";
		float X = (float) 0.0;
		float Y = (float) 0.0;
		float Z = (float) 0.0;
		int velocidad=0;
		boolean periscopi=false;
		boolean motor=false;
		boolean amarrat=false;
		
		int fase = 0;
		boolean correcto=true;
		
		do {
			System.out.println("[1] - MANEJO DEL SQUAD");
			System.out.println("1. Dar de alta submarino");
			System.out.println("2. Liquidar submarino");
			System.out.println("3. Salir");
			
			opcio = sc.nextInt();
			
			switch(opcio) {
				case 1:
					correcto = true;
					fase = 0;
					scTMP = new Scanner(System.in);
					while(correcto == true) {
						switch(fase) {
							case 0:
								System.out.println("[COORDENADAS]___________");
								System.out.println("Escribe la coordenada X (Horizontal): 0 a 1000");
								X = scTMP.nextInt();
								if(X >= 0 && X <= 1000) {
									fase++;
								} else {
									Audio.sonidoAlerta();
									MostrarMensajeAlerta("[!] Las coordenadas deben estar entre 0 y 1000");
								}
								break;
							case 1:
								System.out.println("Escribe la coordenada Y (Vertical): 0 a 1000");
								Y = scTMP.nextInt();
								if(Y >= 0 && Y <= 1000) {
									fase++;
								} else {
									MostrarMensajeAlerta("[!] Las coordenadas deben estar entre 0 y 1000");
								}
								break;
							case 2:
								System.out.println("Escribe la coordenada Y (Profundidad): 0 a -2500");
								Z = scTMP.nextInt();
								if(Z <= 0 && Z >= -2500) {
									if(!squadJugador.ExisteAlgoPosicion(X, Y, Z)) {
										fase++;
									}
								} else {
									MostrarMensajeAlerta("[!] Las coordenadas deben estar entre 0 y -2500");
								}
								break;
							case 3:
								System.out.println("[MODELO]___________");
								System.out.println("Escribe la matricula del submarino:");
								Matricula = sc.next();

								if(squadJugador.ExisteSubmarino(Matricula) == -1) {
									fase++;
								} else {
									MostrarMensajeAlerta("[!] Ya existe un submarino con esa matricula");
								}
								break;
							case 4:
								System.out.println("Escribe la marca:");
								Marca = sc.next();
								fase++;
								break;
							case 5:
								System.out.println("Escribe el Modelo:");
								Modelo = sc.next();
								squadJugador.afegirSubmarino(Marca,Modelo, Matricula, X ,Y ,Z, velocidad , periscopi, motor, amarrat,CData.obtenerFecha());
								System.out.println("Submarino insertado correctamente! Ya esta en el GRID.");
								correcto = false;
								break;
								
						}
					}
					break;
				case 2:
					System.out.println("Escribe la matricula del submarino a liquidar:");
					Matricula = sc.next();
					int id_submarino = squadJugador.ExisteSubmarino(Matricula);
					if(id_submarino >= 0) {
						System.out.println("El submarino hizo BOOOM!");
						squadJugador.BorrarSubmarino(id_submarino);
					}
					break;
			}
			
		} while(opcio != 3);
		Menu();
	}
	
	public static void MostrarMensajeAlerta(String msg) {
		Audio.sonidoAlerta();
		System.out.println(msg);
	}
	
	
	public static void ManejarSubmarino() {
		sc = new Scanner(System.in);
		String opcio="";
		String Matricula = "";
		int SubmarinoEscoger = -1;
		
		
		do {
			System.out.println("[2] - MANEJO DEL SUBMARINO");
			System.out.println("Escribe la matricula del submarino a controlar");
			squadJugador.MostrarListadoSubmarinos();
			Matricula = sc.next();
			
			int id_submarino = squadJugador.ExisteSubmarino(Matricula);
			if(id_submarino >= 0) {
				while (!opcio.equals("m")) {
					System.out.println("[2]["+Matricula+"/ID: "+id_submarino+"] -PANEL DE CONTROL DEL SUBMARINO");
					System.out.println("a. Encender motor");
					System.out.println("b. Apagar motor");
					System.out.println("c. Acelerar");
					System.out.println("d. Frenar");
					System.out.println("e. Abrir/Cerrar compuertas inundacion");
					System.out.println("f. Activar/Desactivar bombas de inundacion");
					System.out.println("g. Incrementar profundidad (Bajar)");
					System.out.println("h. Disminuir profundidad (Subir)");
					System.out.println("i. Subir/bajar periscopio");
					System.out.println("j. Establecer rumbo (0 a 360º) ");
					System.out.println("k. Posicionar X/Y");
					System.out.println("l. Amarrar");
					System.out.println("m. Salir");
					opcio = sc.next();
					
					switch(opcio) {
						case "a":
							System.out.println("-> Encendiendo motor");
							squadJugador.ModificarValoresInt(opcio.toString(),1,id_submarino);
							break;
						case "b":
							System.out.println("-> Apagando motor");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "c":
							System.out.println("-> Aumentando velocidad en 1m/s");
							squadJugador.ModificarValoresInt(opcio.toString(),1,id_submarino);
							break;
						case "d":
							System.out.println("-> Reduciendo velocidad en 1m/s");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "e":
							System.out.println("-> Abriendo/cerrando las compuertas de inundacion");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "f":
							System.out.println("-> Activando/desactivando las bombas de inundacion");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "g":
							System.out.println("-> Incrementando profundidad (Bajar)");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "h":
							System.out.println("-> Disminuiendo profundidad (Subir)");
							squadJugador.ModificarValoresInt(opcio.toString(),1,id_submarino);
							break;
						case "i":
							System.out.println("-> Activando/Desactivando el periscopio");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
						case "j":
							System.out.println("-> Escribe el nuevo rumbo del submarino (0 a 360º) ");
							int nuevoRumbo;
							nuevoRumbo = sc.nextInt();
							squadJugador.ModificarValoresInt(opcio.toString(),nuevoRumbo,id_submarino);
							break;
						case "k":
							boolean repetirPos = true;
							while(repetirPos) {
								System.out.println("-> La nueva posicion (X) ");
								int nuevaX;
								nuevaX = sc.nextInt();
								
								System.out.println("-> La nueva posicion (Y) ");
								int nuevaY;
								nuevaY = sc.nextInt();
								
								float Z = squadJugador.ObtenerPosicionZ(id_submarino);
								
								if(!squadJugador.ExisteAlgoPosicion(nuevaX, nuevaY, Z)) {
									squadJugador.MoverCoordenadas(id_submarino,nuevaX,nuevaY);
									System.out.println("-> El submarino se ha desplazado a esa posicion");
									repetirPos = false;
								} else {
									MostrarMensajeAlerta("[!] Ya existe un submarino en esas coordenadas");
								}
							}
							break;
						case "l":
							System.out.println("-> Amarrando/Desmarrando submarino");
							squadJugador.ModificarValoresInt(opcio.toString(),0,id_submarino);
							break;
					}
				}	
				SubmarinoEscoger = -1;
				id_submarino = -2;
			} else {
				MostrarMensajeAlerta("El submarino no existe");
			}
		} while(SubmarinoEscoger != -1);
		
		ComprobarEstadoMarino();
		Menu();
	}
	
	public static void MostrarEstadoMar() {
		squadJugador.mostrarInfoSubmarinos();
		squadJugador.comprobarColisionSubmarinos();
		Menu();
	}
	
	public static void ComprobarEstadoMarino(){
		System.out.println("Comprobando estado marino...");
		squadJugador.comprobarEstadoSubmarinos();
		Menu();
	}
	

}
