
import java.io.*;
import javazoom.jl.player.Player;

public class Audio {
	
	public static void playClip() {
		//para que se pueda repetir siempre
		final boolean loopMusica = true;
		
		
		/*
		* Quitado el audio por temas de copyright
		new Thread() {

			  @Override
			  public void run() {
				 //Repetimos la musica para tus oidos
				  do
			        {
			    	  //Pokemon colosseum FTW
			    	  //Si estas en contra, diselo a ludicolo y Discal!
					  
					  //Speed Racer tambien se merece su espacio
					  String strAl = "";
					  Random al = new Random();
					  switch(al.GenerarAleatorio(1, 8)) {
					  case 1:
						  strAl = "creeper.mp3";
						  break;
					  case 2:
						  strAl = "easyhouse.mp3";
						  break;
					  case 3:
						  strAl = "cheeky.mp3";
						  break;
					  case 4:
						  strAl = "fartogo.mp3";
						  break;
					  case 5:
						  strAl = "cake.mp3";
						  break;
					  case 6:
						  strAl = "digitalwerck.mp3";
						  break;
					  case 7:
						  strAl = "soitpop.mp3";
						  break;
					  case 8:
						  strAl = "speed.mp3";
						  break;
					  }
					  
					  //Leer el archivo y utilizando la libreria jl1.0
					  try(FileInputStream fis = new FileInputStream(strAl)){
					    	  new Player(fis).play();
					        
					    }catch(Exception e){System.out.println(e);}
			        }while(loopMusica); 
			  }
			}.start();
		*/
	}
	
	public static void sonidoAlerta() {
		//Mostrar un sonido de alerta al hacer algo mal
		
		new Thread() {

			  @Override
			  public void run() {
				  String strAl = "alerta.mp3";				  
				  //Leer el archivo y utilizando la libreria jl1.0
				  try(FileInputStream fis = new FileInputStream(strAl)){
					  new Player(fis).play();
				        
				  }catch(Exception e){System.out.println(e);}
			       
			  }
			}.start();
		
	}
}
