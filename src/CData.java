import java.text.SimpleDateFormat;
import java.util.Date;
public class CData {

    public static String obtenerFecha() {
        Date ahora = new Date();
        SimpleDateFormat generarFecha = new SimpleDateFormat("yyyy-MM-dd");
        return generarFecha.format(ahora);
    }
    
    public static String ConvertirFechaUSA(String fecha) {
    	String fechaNueva = "";
    	String[] separador = fecha.split("-");
    	fechaNueva = separador[2]+"-"+separador[1]+"-"+separador[0];
    	return fechaNueva;
    }

}