import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Squad {
	private String identificador;
	private int capacitat=5;
	private Submarinos[] llistaSubmarinos;
	
	private int submarinosTotales = 0;
	private int numAl=0;

	
	public Squad(String identificador) {
		this.identificador=identificador;
		llistaSubmarinos = new Submarinos[this.capacitat];
	}
	
	public void afegirSubmarino(String Marca,String Modelo, String Matricula, float X,float Y,float Z,int velocidad, boolean periscopi, boolean motor, boolean amarrat,String Fecha) {	
		//Seteamos el nuevo submarino
		Submarinos aTmp =  new Submarinos(Marca,Modelo, Matricula, X, Y,Z,velocidad,periscopi, motor, amarrat,Fecha);
		
		if(this.submarinosTotales <= this.capacitat) {
			//Insertamos normal
			llistaSubmarinos[this.numAl]=aTmp;
		} else {
			//Insertamos en los espacios libres
			boolean trobat=false;
			int idx=0;
			while(!(trobat) && (idx < capacitat)) {
				if(llistaSubmarinos[idx].getMatricula() == null) {
					llistaSubmarinos[idx]=aTmp;
					
				}
				idx++;
			}
		}
		
		this.submarinosTotales++;
		this.numAl++;
		if(this.numAl > this.capacitat) {
			this.numAl = this.capacitat;
		}
	}
	
	public int getSubmarinosInsertados() {
		return this.submarinosTotales;
	}
	
	public int getCapacidadHangar() {
		return this.capacitat;
	}
	
	public void mostrarInfoSubmarinos() {
		System.out.println("Jugador: "+this.identificador);
		System.out.println("Submarins:"+this.numAl+"/"+this.capacitat);
		System.out.println("____SUBMARINS_____");
		
		for(int i=0; i<numAl;i++) {
			if(llistaSubmarinos[i].getMatricula() != null) {
				System.out.println("[Matricula]["+this.identificador+"."+llistaSubmarinos[i].getMatricula()+"]_________");
				System.out.println("Marca: "+llistaSubmarinos[i].getMarca());
				System.out.println("Modelo: "+llistaSubmarinos[i].getModelo());
				System.out.println("X: "+llistaSubmarinos[i].getX() + "/ Y: "+llistaSubmarinos[i].getY()+" / Z: "+llistaSubmarinos[i].getZ());
				System.out.println("Velocidad: "+llistaSubmarinos[i].getVelocidad()+" m/s");
				System.out.println("Periscopi: "+llistaSubmarinos[i].getPeriscopi());
				System.out.println("Motor: "+llistaSubmarinos[i].getMotor());
				System.out.println("Amarrat: "+llistaSubmarinos[i].getAmarrat());
				System.out.println("Fecha de botadura: "+CData.ConvertirFechaUSA(llistaSubmarinos[i].getFechaCreacion()));
				System.out.println("Rumbo: "+llistaSubmarinos[i].getRumbo()+"º");
				System.out.println("Compuertas inundacion: "+llistaSubmarinos[i].getCompuertas());
				System.out.println("Bomba inundacion: "+llistaSubmarinos[i].getBomba());
				System.out.println("_________________________________");
			}
		}
	}
	
	public void MostrarListadoSubmarinos() {
		for(int i=0; i<numAl;i++) {
			if(llistaSubmarinos[i].getMatricula() != null) {
				System.out.println("[ID:"+i+"]_________________________________");
				System.out.println("Marca: "+llistaSubmarinos[i].getMarca()+"/ Modelo: "+llistaSubmarinos[i].getModelo());
				System.out.println("Matricula: "+llistaSubmarinos[i].getMatricula());
				System.out.println("X: "+llistaSubmarinos[i].getX()+"/ Y:"+llistaSubmarinos[i].getY()+"/ Z: "+llistaSubmarinos[i].getZ());
			}
		}
	}
	
	public void comprobarColisionSubmarinos() {
		System.out.println("[COMPROBADOR DE COLISIONES]_________________________________");
		int peligros = 0;
		for(int i=0; i<numAl;i++) {
			if(llistaSubmarinos[i].getMatricula() != null) {
				for(int d=0; d<numAl;d++) {
					//Comprobamos que no es el mismo submarino
					if(llistaSubmarinos[d].getMatricula() != null && d != i) {
						float diferencia_x = Math.abs(llistaSubmarinos[i].getX()-llistaSubmarinos[d].getX());
						float diferencia_y = Math.abs(llistaSubmarinos[i].getY()-llistaSubmarinos[d].getY());
						float diferencia_z = Math.abs(Math.abs(llistaSubmarinos[i].getZ())-Math.abs(llistaSubmarinos[d].getZ()));
						if(diferencia_x <= 50 && diferencia_z <= 500 ) {
							System.out.println("Submarinos: "+llistaSubmarinos[i].getMatricula()+" y "+llistaSubmarinos[d].getMatricula()+" A punto de colisionar-> Misma X (Distancia X: "+diferencia_x+" / Distancia Z: "+diferencia_z+")");
						}
						if(diferencia_y <= 50 && diferencia_z <= 500 ) {
							System.out.println("Submarinos: "+llistaSubmarinos[i].getMatricula()+" y "+llistaSubmarinos[d].getMatricula()+" A punto de colisionar-> Misma Y (Distancia Y: "+diferencia_y+" / Distancia Z: "+diferencia_z+")");
						}
					}
				}
			}
		}
		if(peligros == 0) {
			System.out.println("Todo en orden. No hay peligros de colision a la vista");
		}
	}
	
	public void comprobarEstadoSubmarinos() {
		for(int i=0; i<numAl;i++) {
			if(llistaSubmarinos[i].getMatricula() != null) {
				if(llistaSubmarinos[i].getAmarrat() == true) {
					BorrarSubmarino(i);
				} else if(llistaSubmarinos[i].getY() > 1000 || llistaSubmarinos[i].getX() > 1000 || llistaSubmarinos[i].getY() <= -1 || llistaSubmarinos[i].getX() <= -1 ||  llistaSubmarinos[i].getZ() < -2500) {
					//System.out.println("Borrando submarino: "+i);
					BorrarSubmarino(i);
				}
			}
		}
	}
	
	
	public int ExisteSubmarino(String MatriculaBuscar) {
		boolean trobat=false;
		int idSubmarino=-1;
		int idx=0;

		while(!(trobat) && (idx < capacitat) && (idx < this.numAl)) {
			if(llistaSubmarinos[idx].getMatricula() != null) {
				if(llistaSubmarinos[idx].getMatricula().equalsIgnoreCase(MatriculaBuscar)) {
					trobat=true;
					idSubmarino = idx;
				} 
			}
			idx++;
		}
		return (idSubmarino);
	}
	
	public boolean ExisteAlgoPosicion(float X,float Y,float Z) {
		boolean existe=false;
		int idx=0;

		while(!(existe) && (idx < capacitat) && (idx < this.numAl)) {
			if(llistaSubmarinos[idx].getMatricula() != null) {
				if(llistaSubmarinos[idx].getX() == X && llistaSubmarinos[idx].getY() == Z && llistaSubmarinos[idx].getZ() == Z) {
					existe=true;
				} 
			}
			idx++;
		}
		
		return existe;
	}
	
	public float ObtenerPosicionZ(int idSubmarino) {
		float Z = (float) 1.0;
		if(llistaSubmarinos[idSubmarino].getMatricula() != null) {
			Z = llistaSubmarinos[idSubmarino].getZ();
		}
		return Z;
	}
	
	public void MoverCoordenadas(int idSubmarino,float X,float Y) {
		if(llistaSubmarinos[idSubmarino].getMatricula() != null) {
			llistaSubmarinos[idSubmarino].setX(X);
			llistaSubmarinos[idSubmarino].setY(Y);
			llistaSubmarinos[idSubmarino].Syncronizar();
		}
	}

	
	public void BorrarSubmarino(int idSubmarino) {
		if(llistaSubmarinos[idSubmarino].getMatricula() != null) {
			llistaSubmarinos[idSubmarino].Borrar();
			llistaSubmarinos[idSubmarino].setMarca(null);
			llistaSubmarinos[idSubmarino].setModelo(null);
			llistaSubmarinos[idSubmarino].setMatricula(null);
			llistaSubmarinos[idSubmarino].setX((float) 0.0);
			llistaSubmarinos[idSubmarino].setY((float) 0.0);
			llistaSubmarinos[idSubmarino].setZ((float) 0.0);
			llistaSubmarinos[idSubmarino].setVelocidad(0);
			llistaSubmarinos[idSubmarino].setPeriscopi(false);
			llistaSubmarinos[idSubmarino].setMotor(false);
			llistaSubmarinos[idSubmarino].setAmarrat(false);
			this.submarinosTotales--;
		}
	}
	
	public void CargarSubmarinos(String json) {
		Object obj=JSONValue.parse(json);
		
		JSONArray array=(JSONArray)obj;
		for(int i = 0; i< array.size(); i++) {
			int IDArray = i;
			JSONObject SubMarino=(JSONObject)array.get(IDArray);
			afegirSubmarino((String) SubMarino.get("Marca"),(String) SubMarino.get("Modelo"),(String) SubMarino.get("Matricula"),Float.parseFloat((String)SubMarino.get("X")),Float.parseFloat((String)SubMarino.get("Y")),Float.parseFloat((String)SubMarino.get("Z")),Integer.parseInt((String)SubMarino.get("velocidad")), Boolean.parseBoolean((String)SubMarino.get("periscopi")),Boolean.parseBoolean((String)SubMarino.get("motor")), Boolean.parseBoolean((String)SubMarino.get("amarrat")),(String) SubMarino.get("Fecha"));
		}
	}
	
	public void ActualizarPosicionSubmarinos(String json) {
		Object obj=JSONValue.parse(json);
		
		JSONArray array=(JSONArray)obj;
		for(int i = 0; i< array.size(); i++) {
			int IDArray = i;
			JSONObject SubMarino=(JSONObject)array.get(IDArray);
			int idSubmarino = ExisteSubmarino((String) SubMarino.get("Matricula"));
			if(idSubmarino >= 0) {
				MoverCoordenadas(idSubmarino,Float.parseFloat((String)SubMarino.get("X")),Float.parseFloat((String)SubMarino.get("Y")));
			}
		}
	}

	
	public void ModificarValoresInt(String idValor, int nuevo_valor,int idSubmarino) {
		if(idValor.equals("a") || idValor.equals("b")) {
			boolean boleano = llistaSubmarinos[idSubmarino].getMotor();
			if(nuevo_valor == 1 && boleano == false) {
				boleano = true;
			} else {
				//Control de errores
				if(boleano == true && nuevo_valor == 1) {
					Juego.MostrarMensajeAlerta("[!] El submarino ya tenia el motor encendido.");
				} 
				else if(boleano == false && nuevo_valor == 0) {
					Juego.MostrarMensajeAlerta("[!] El submarino ya tenia el motor parado.");
				}
			}
			llistaSubmarinos[idSubmarino].setMotor(boleano);
		}
		else if(idValor.equals("c") || idValor.equals("d")) {
			int velocidad = llistaSubmarinos[idSubmarino].getVelocidad();
			boolean estado_motor = llistaSubmarinos[idSubmarino].getMotor();
			if(estado_motor == true) {
				//SI el valor es 1, aumentamos SIEMPRE
				if(nuevo_valor == 1) {
					velocidad +=1;
				} else {
					//SIno, como no hay marcha atras no puede reducir
					if(velocidad > 0) {
						velocidad -=1;
						if(velocidad == 0) {
							System.out.println("El submarino se ha parado.");
						}
					} else if(velocidad == 0) {
						Juego.MostrarMensajeAlerta("[!] El submarino no puede frenar, esta a 0m/s");
					}
				}
				llistaSubmarinos[idSubmarino].setVelocidad(velocidad);
			} else {
				Juego.MostrarMensajeAlerta("[!] No puedes avanzar/retroceder si el motor esta parado.");
			}
			
		}
		else if(idValor.equals("e")) {
			boolean estado_compuertas = llistaSubmarinos[idSubmarino].getCompuertas();
			if(estado_compuertas == false) {
				estado_compuertas = true;
				System.out.println("Abriendo las compuertas de inundacion");
			} else {
				estado_compuertas = false;
				System.out.println("Cerrando las compuertas de inundacion");
			}
			llistaSubmarinos[idSubmarino].setCompuertas(estado_compuertas);
		}
		else if(idValor.equals("f")) {
			boolean estado_compuertas = llistaSubmarinos[idSubmarino].getCompuertas();
			if(estado_compuertas == true) {
				boolean estado_bombas = llistaSubmarinos[idSubmarino].getBomba();
				if(estado_bombas == false) {
					estado_bombas = true;
					System.out.println("Activando las bombas de inundacion");
				} else {
					estado_bombas = false;
					System.out.println("Cerrando las bombas de inundacion");
				}
				llistaSubmarinos[idSubmarino].setBomba(estado_bombas);
			} else {
				Juego.MostrarMensajeAlerta("[!] No puedes abrir/cerrar las bombas de inundacion si las compuertas de inundacion no estan abiertas");
			}
		}
		//para bajar activadas las compuertas
		//para subir activadas las bombas
		else if(idValor.equals("g") || idValor.equals("h")) {
			if(nuevo_valor == 0) {
				//Incrementando profundidad (bajar)
				boolean estado_compuertas = llistaSubmarinos[idSubmarino].getCompuertas();
				boolean estado_periscopio = llistaSubmarinos[idSubmarino].getPeriscopi();
				int velocidad = llistaSubmarinos[idSubmarino].getVelocidad();
				float Z = llistaSubmarinos[idSubmarino].getZ();
				if(estado_compuertas == true) {
					if(velocidad <= 20) {
						Z -=10;
						if(Z < -20 && estado_periscopio == true) {
							Z = -20;
							Juego.MostrarMensajeAlerta("[!] No se peude bajar mas de 20 metros si no desactivas el periscopio");
						}
						System.out.println("Incrementando profundidad. [Nueva Z: "+Z+"]");
						llistaSubmarinos[idSubmarino].setZ(Z);
					} else {
						Juego.MostrarMensajeAlerta("[!] No puedes desplazarte abajo si la velocidad es superior a 20 m/s");
					}
					
				} else {
					Juego.MostrarMensajeAlerta("[!] No puedes desplazarte abajo si no tienes abierto las compuertas");
				}
			} else if(nuevo_valor == 1) {
				//Disminuiendo profundidad (subir)
				boolean estado_bombas = llistaSubmarinos[idSubmarino].getBomba();
				float Z = llistaSubmarinos[idSubmarino].getZ();
				if(Z < 0) {
					if(estado_bombas == true) {
						Z +=10;
						if(Z > 0) {
							Z = 0;
						}
						System.out.println("Disminuiendo profunidad. [Nueva Z: "+Z+"]");
						llistaSubmarinos[idSubmarino].setZ(Z);
					} else {
						Juego.MostrarMensajeAlerta("[!] No puedes desplazarte arriba si no tienes abierto las bombas");
					}
				} else {
					Juego.MostrarMensajeAlerta("[!] No puedes disminuir profunidad, estas en tierra (Z: 0), no eres un avion");
				}
				
			}
			
		}
		//Periscopio
		else if(idValor.equals("i")) {
			boolean estado_periscopio = llistaSubmarinos[idSubmarino].getPeriscopi();
			float Z = llistaSubmarinos[idSubmarino].getZ();
			if(Z <= 0 && Z >= -5) {
				if(estado_periscopio == false) {
					estado_periscopio = true;
					System.out.println("Activando el periscopio");
				} else {
					estado_periscopio = false;
					System.out.println("Desactivando el periscopio");
				}
				llistaSubmarinos[idSubmarino].setPeriscopi(estado_periscopio);
			} else {
				Juego.MostrarMensajeAlerta("[!] Solo se puede activar/desactivar el periscopio cuando la profundida sea entre 0 y -5m");
			}
			

		}
		//Rumbo
		else if(idValor.equals("j")) {
			if(nuevo_valor >= 0 && nuevo_valor <= 360) {
				llistaSubmarinos[idSubmarino].setRumbo(nuevo_valor);
				System.out.println("Nuevo rumbo establecido a: "+nuevo_valor+"º");
			} else {
				Juego.MostrarMensajeAlerta("[!] El rumbo tiene que ser entre 0 y 360º");
			}
		}
		//Amarrar/desmarrar
		else if(idValor.equals("l")) {
			boolean estado_amarrar = llistaSubmarinos[idSubmarino].getAmarrat();
			float Z = llistaSubmarinos[idSubmarino].getZ();
			int velocidad = llistaSubmarinos[idSubmarino].getVelocidad();
			if(Z == 0) {
				if(velocidad >= 0 && velocidad <= 2) {
					if(estado_amarrar == true) {
						estado_amarrar = false;
						System.out.println("Desmarrando submarino");
					} else {
						System.out.println("Amarrando submarino");
						estado_amarrar = true;
					}
					llistaSubmarinos[idSubmarino].setAmarrat(estado_amarrar);
				} else {
					Juego.MostrarMensajeAlerta("[!] No puedes amarrar un submarino si su velocidad no esta entre 0 y 2 m/s");
				}
				
			} else {
				Juego.MostrarMensajeAlerta("[!] Para amarrar un submarino debe estar en profunidad 0 (tierra)");
			}
			
			
		}
		
		llistaSubmarinos[idSubmarino].Syncronizar();
		
	}
}
