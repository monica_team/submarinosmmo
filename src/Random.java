import java.util.concurrent.ThreadLocalRandom;

public class Random {
	public int GenerarAleatorio(int min, int max) {


	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
		int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
	    return randomNum;
	}
}
